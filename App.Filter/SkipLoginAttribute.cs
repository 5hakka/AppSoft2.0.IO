﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*!
* 文件名称：跳过登录特性，只要在控制器或Action贴此特性就可以跳过登录验证
* 文件作者：新生帝
* 编写日期：2016-02-27
* 版权所有：中山赢友网络科技有限公司
* 企业官网：http://www.winu.net
* 开源协议：GPL v2 License
* 文件描述：一切从简，只为了更懒！
*/
namespace App.Filter
{
    public class SkipLoginAttribute : Attribute
    {
    }
}
